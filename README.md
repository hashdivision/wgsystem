Worldman Games System
=====================

Status
------

**Production** branch: [![production](https://gitlab.com/hashdivision/wgsystem/badges/production/pipeline.svg)](https://gitlab.com/hashdivision/wgsystem/commits/production) | [![production](https://gitlab.com/hashdivision/wgsystem/badges/production/coverage.svg)](https://gitlab.com/hashdivision/wgsystem/commits/production)

**Pre-production** branch: [![master](https://gitlab.com/hashdivision/wgsystem/badges/master/pipeline.svg)](https://gitlab.com/hashdivision/wgsystem/commits/master) | [![master](https://gitlab.com/hashdivision/wgsystem/badges/master/coverage.svg)](https://gitlab.com/hashdivision/wgsystem/commits/master)

**Development** branch: [![development](https://gitlab.com/hashdivision/wgsystem/badges/development/pipeline.svg)](https://gitlab.com/hashdivision/wgsystem/commits/development) | [![development](https://gitlab.com/hashdivision/wgsystem/badges/development/coverage.svg)](https://gitlab.com/hashdivision/wgsystem/commits/development)

Repositories
------------

- **PyPI repository**: [wg-system](https://pypi.org/project/wg-system/)

- **npm repository**: *coming soon*

Description
-----------

Backend and frontend (*only logic part*) of system that is used internally for managing [Worldman Games](http://wmgames.ee) events.

Written in **Python 3.x** (*backend*) and **TypeScript** (*frontend*) and has 100% of code type-annotated and *covered by tests (**not yet**)*.

# Contributions (issues or merge requests) are welcome! :)