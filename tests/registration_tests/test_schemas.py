import string
import random
import asynctest
from starlette.formparsers import FormData
from typesystem.base import ValidationError
from wg_system.registration.schemas import Register, EMPTY_REGISTER_DATA
from wg_system.registration.resources import DATABASE, DATABASE_METADATA, DATABASE_ENGINE

def rand_str(maximum_letters: int) -> str:
    return ''.join([random.choice(string.ascii_letters) for n in range(random.randint(1, maximum_letters))])

def rand_str_specific_len(length: int) -> str:
    return ''.join([random.choice(string.ascii_letters) for n in range(length)])

def generate_perfect_data() -> dict:
    perfect_data = EMPTY_REGISTER_DATA.copy()
    perfect_data["first_name"] = rand_str(64)
    perfect_data["last_name"] = rand_str(64)
    perfect_data["email"] = f"{rand_str(84)}@{rand_str(84)}.{rand_str(84)}"
    perfect_data["age"] = random.randint(1, 100)
    perfect_data["gender"] = rand_str(16)
    perfect_data["language"] = random.choice(["EST", "ENG", "RUS"])
    return perfect_data

class SchemasTestCase(asynctest.TestCase):
    use_default_loop = True
    forbid_get_event_loop = False

    async def setUp(self):
        DATABASE_METADATA.create_all(DATABASE_ENGINE)
        if not DATABASE.is_connected:
            await DATABASE.connect()

    async def tearDown(self):
        DATABASE_METADATA.drop_all(DATABASE_ENGINE)
        if DATABASE.is_connected:
            await DATABASE.disconnect()

    def test_register_parse_form_success(self):
        # Generate random, but perfect form to be parsed
        form = FormData([
            ("first_name", rand_str(64)),
            ("last_name", rand_str(64)),
            ("email", f"{rand_str(84)}@{rand_str(84)}.{rand_str(84)}"),
            ("age", random.randint(1, 100)),
            ("gender", rand_str(16)),
            ("language", random.choice(["EST", "ENG", "RUS"]))
        ])

        data = Register.parse_form(form)
        self.assertIsNotNone(Register.validate(data))

    async def test_register_is_email_unique(self):
        data = generate_perfect_data()
        email = data["email"]
        register = Register.validate(data)
        self.assertTrue(await register.is_email_unique())

        registrant = await register.create_registrant()
        self.assertIsNotNone(registrant)
        self.assertEqual(registrant.email, email)
        self.assertFalse(await register.is_email_unique())

    def test_register_validation_success(self):
        # Check if perfect data is validated
        self.assertIsNotNone(Register.validate(generate_perfect_data()))

        # Check if maximum length for email is not rejected
        data_with_max_email_len = generate_perfect_data()
        data_with_max_email_len["email"] = f"{rand_str_specific_len(84)}@{rand_str_specific_len(84)}.{rand_str_specific_len(84)}"
        self.assertIsNotNone(Register.validate(data_with_max_email_len))

    def test_register_validation_fail(self):
        # Check that empty register data validation fails
        with self.assertRaises(ValidationError):
            Register.validate(EMPTY_REGISTER_DATA)

        # Check if validation fails without required fields
        data_without_first_name = generate_perfect_data()
        data_without_first_name["first_name"] = None
        with self.assertRaises(ValidationError):
            Register.validate(data_without_first_name)

        data_without_last_name = generate_perfect_data()
        data_without_last_name["last_name"] = None
        with self.assertRaises(ValidationError):
            Register.validate(data_without_last_name)

        data_without_email = generate_perfect_data()
        data_without_email["email"] = None
        with self.assertRaises(ValidationError):
            Register.validate(data_without_email)

        data_without_age = generate_perfect_data()
        data_without_age["age"] = None
        with self.assertRaises(ValidationError):
            Register.validate(data_without_age)

        data_without_language = generate_perfect_data()
        data_without_language["language"] = None
        with self.assertRaises(ValidationError):
            Register.validate(data_without_language)

    async def test_register_create_registrant(self):
        register = Register.validate(generate_perfect_data())
        registrant = await register.create_registrant()
        self.assertIsNotNone(registrant)
        self.assertEqual(registrant.id, 1)