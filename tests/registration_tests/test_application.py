import asynctest
from starlette.status import HTTP_200_OK
from starlette.testclient import TestClient
from starlette.requests import Request as StarletteRequest
from starlette.responses import PlainTextResponse
from wg_system.registration import REGISTRATION

@REGISTRATION.route('/works')
async def testing_endpoint(request: StarletteRequest) -> PlainTextResponse:
    return PlainTextResponse('works')

class ApplicationTestCase(asynctest.TestCase):
    def test_application(self):
        with TestClient(REGISTRATION) as client:
            result = client.get('/works')
            self.assertEqual(result.status_code, HTTP_200_OK)
            self.assertEqual(result.text, 'works')
