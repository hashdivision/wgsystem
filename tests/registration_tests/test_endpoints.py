import asyncio
import string
import random
import asynctest
from starlette.testclient import TestClient
from wg_system.registration.schemas import EMPTY_REGISTER_DATA
from wg_system.registration.application import REGISTRATION
from wg_system.registration.settings import URL_NOT_FOUND_PAGE, URL_WELCOME_PAGE
from wg_system.registration.resources import DATABASE_METADATA, DATABASE_ENGINE

def rand_str(maximum_letters: int) -> str:
    return ''.join([random.choice(string.ascii_letters) for n in range(random.randint(1, maximum_letters))])

def rand_str_specific_len(length: int) -> str:
    return ''.join([random.choice(string.ascii_letters) for n in range(length)])

def generate_perfect_data() -> dict:
    perfect_data = EMPTY_REGISTER_DATA.copy()
    perfect_data["first_name"] = rand_str(64)
    perfect_data["last_name"] = rand_str(64)
    perfect_data["email"] = f"{rand_str(84)}@{rand_str(84)}.{rand_str(84)}"
    perfect_data["age"] = random.randint(1, 100)
    perfect_data["gender"] = rand_str(16)
    perfect_data["language"] = random.choice(["EST", "ENG", "RUS"])
    perfect_data["privacy_policy_consent"] = 1
    return perfect_data

class EndpointsTestCase(asynctest.TestCase):
    use_default_loop = False
    forbid_get_event_loop = False

    def setUp(self):
        DATABASE_METADATA.create_all(DATABASE_ENGINE)

    def tearDown(self):
        DATABASE_METADATA.drop_all(DATABASE_ENGINE)

    def test_register_v0(self):
        data = generate_perfect_data()

        # Check that everything should work as soon as data is ok
        with TestClient(REGISTRATION) as client:
            response = client.post("/v0/register", data=data)
            self.assertEqual(response.json(), {'success' : True})

        # Check that same email second time will not work
        with TestClient(REGISTRATION) as client:
            response = client.post("/v0/register", data=data)
            self.assertEqual(response.json(), {'email' : 'Already registered!'})

        # Check that it should fail as soon as data is wrong or required fields are missing
        wrong_data = data.copy()
        wrong_data['privacy_policy_consent'] = 0
        with TestClient(REGISTRATION) as client:
            response = client.post("/v0/register", data=wrong_data)
            self.assertNotEqual(response.json(), {'success' : True})

        wrong_data = data.copy()
        wrong_data['privacy_policy_consent'] = None
        with TestClient(REGISTRATION) as client:
            response = client.post("/v0/register", data=wrong_data)
            self.assertNotEqual(response.json(), {'success' : True})

        wrong_data = data.copy()
        wrong_data['email'] = 'wrong@mail'
        with TestClient(REGISTRATION) as client:
            response = client.post("/v0/register", data=wrong_data)
            self.assertNotEqual(response.json(), {'success' : True})

        wrong_data = data.copy()
        wrong_data['first_name'] = None
        with TestClient(REGISTRATION) as client:
            response = client.post("/v0/register", data=wrong_data)
            self.assertNotEqual(response.json(), {'success' : True})

        wrong_data = data.copy()
        wrong_data['last_name'] = None
        with TestClient(REGISTRATION) as client:
            response = client.post("/v0/register", data=wrong_data)
            self.assertNotEqual(response.json(), {'success' : True})

        wrong_data = data.copy()
        wrong_data['email'] = None
        with TestClient(REGISTRATION) as client:
            response = client.post("/v0/register", data=wrong_data)
            self.assertNotEqual(response.json(), {'success' : True})

        wrong_data = data.copy()
        wrong_data['age'] = None
        with TestClient(REGISTRATION) as client:
            response = client.post("/v0/register", data=wrong_data)
            self.assertNotEqual(response.json(), {'success' : True})

    def test_confirm_v0(self):
        data = generate_perfect_data()

        # Check that everything should work as soon as data is ok
        with TestClient(REGISTRATION) as client:
            response = client.post("/v0/register", data=data)
            self.assertEqual(response.json(), {'success' : True})
            response = client.get("/v0/confirm/1234567890")
            self.assertTrue(str(URL_WELCOME_PAGE) in response.url)
            response = client.get(f"/v0/confirm/{rand_str(64)}")
            self.assertEqual(response.url, URL_NOT_FOUND_PAGE)
