import string
import random
import asynctest
from secrets import token_urlsafe
from wg_system.registration.mail import send_confirmation_email, send_welcome_email
from wg_system.registration.schemas import Register, EMPTY_REGISTER_DATA
from wg_system.registration.resources import DATABASE, DATABASE_METADATA, DATABASE_ENGINE
from wg_system.registration.models import Participant

def rand_str(maximum_letters: int) -> str:
    return ''.join([random.choice(string.ascii_letters) for n in range(random.randint(1, maximum_letters))])

def rand_str_specific_len(length: int) -> str:
    return ''.join([random.choice(string.ascii_letters) for n in range(length)])

def generate_perfect_data() -> dict:
    perfect_data = EMPTY_REGISTER_DATA.copy()
    perfect_data["first_name"] = rand_str(64)
    perfect_data["last_name"] = rand_str(64)
    perfect_data["email"] = f"{rand_str(84)}@{rand_str(84)}.{rand_str(84)}"
    perfect_data["age"] = random.randint(1, 100)
    perfect_data["gender"] = rand_str(16)
    perfect_data["language"] = random.choice(["EST", "ENG", "RUS"])
    return perfect_data

class MailTestCase(asynctest.TestCase):
    use_default_loop = True
    forbid_get_event_loop = False

    async def setUp(self):
        DATABASE_METADATA.create_all(DATABASE_ENGINE)
        if not DATABASE.is_connected:
            await DATABASE.connect()

    async def tearDown(self):
        DATABASE_METADATA.drop_all(DATABASE_ENGINE)
        if DATABASE.is_connected:
            await DATABASE.disconnect()

    async def test_send_confirmation_email(self):
        data = generate_perfect_data()

        register = Register.validate(data)
        registrant = await register.create_registrant()
        await registrant.update(confirmation_code=token_urlsafe(64))
        self.assertTrue(await send_confirmation_email(registrant))

    async def test_send_welcome_email(self):
        data = generate_perfect_data()

        register = Register.validate(data)
        registrant = await register.create_registrant()
        participant = await Participant.objects.create(registrant=registrant, burpees_made=None)
        await send_welcome_email(participant)
