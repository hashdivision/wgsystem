from orm import Model, Integer, String, Boolean, ForeignKey
from . settings import DATABASE_REGISTRANTS_TABLE, DATABASE_PARTICIPANTS_TABLE
from . resources import DATABASE, DATABASE_METADATA

class Registrant(Model):
    __tablename__ = DATABASE_REGISTRANTS_TABLE
    __database__ = DATABASE
    __metadata__ = DATABASE_METADATA

    id = Integer(primary_key=True)
    #: [REGISTRATION FORM | REQUIRED]
    first_name = String(max_length=64, allow_null=False)
    #: [REGISTRATION FORM | REQUIRED]
    last_name = String(max_length=64, allow_null=False)
    #: [REGISTRATION FORM | REQUIRED]
    email = String(max_length=254, index=True, unique=True, allow_null=False)
    #: [REGISTRATION FORM | REQUIRED] What age (s)he is. Used to determine age categories winners.
    age = Integer(allow_null=False)
    #: [REGISTRATION FORM | REQUIRED] Male or Female in different languages. Used to determine gender categories winners.
    gender = String(max_length=16, allow_null=False)
    #: Did (s)he confirmed email by opening URL with confirmation code
    confirmed = Boolean(default=False, allow_null=False)
    #: Random confirmation code. Used in URL that is sent to registrant to confirm email
    confirmation_code = String(max_length=100, index=True, allow_null=True)
    #: [REGISTRATION FORM | HIDDEN]
    language = String(max_length=3, allow_null=False)

class Participant(Model):
    __tablename__ = DATABASE_PARTICIPANTS_TABLE
    __database__ = DATABASE
    __metadata__ = DATABASE_METADATA

    #: Number of place where (s)he starts
    start_number = Integer(primary_key=True)
    #: Link to Registrant that this row is associated with
    registrant = ForeignKey(Registrant)
    #: Did (s)he actually attended the event
    attended = Boolean(default=False, allow_null=False)
    #: How many burpees (s)he made
    burpees_made = Integer(default=0, allow_null=True)
    #: How much (s)he paid for participation
    participation_contribution = Integer(default=0, allow_null=True)
    #: How much (s)he gave to charity directly
    charity_contribution = Integer(default=0, allow_null=True)
