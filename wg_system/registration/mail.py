from base64 import b64encode
from requests_async import post
from .. utils.mail import create_multipart_message, send_email
from . resources import SMTP_SERVER
from . models import Registrant, Participant
from . settings import URL_CONFIRM_BASE, MAILCHIMP_API_KEY, MAILCHIMP_DATACENTER, \
    MAILCHIMP_AUDIENCE_OPTIONS, CONFIRMATION_FROM, URL_CONFIRM_BASE, TESTING

async def send_confirmation_email(registrant: Registrant) -> bool:
    message_body_options = {
        "EST" : (f"""Hei ! Registreerimine on peaaegu valmis, ainult üks samm veel. \
Et saada osaleja number palun kinnita oma registreerimine:""", 
        f"""Hei ! Registreerimine on peaaegu valmis, ainult üks samm veel. 
            Et saada osaleja number palun kinnita oma registreerimine:<br />
            <br />
            <a href="{URL_CONFIRM_BASE}/{registrant.confirmation_code}" class="button">
                Kinnita Registreerimine
            </a><br />
        """),
        "ENG" : (f"""Hey ! It’s almost done, just one more step. \
To get your participant number please confirm your registration:""",
        f"""Hey ! It’s almost done, just one more step. 
            To get your participant number please confirm your registration:<br />
            <br />
            <a href="{URL_CONFIRM_BASE}/{registrant.confirmation_code}" class="button">
                Confirm Registration
            </a><br />"""),
        "RUS" : (f"""Хей ! Регистрация почти окончена, остался один шаг :) \
Чтобы получить номер участника подтверди свою регистрацию, подтверди свою регистрацию:""",
        f"""Хей ! Регистрация почти окончена, остался один шаг :) 
            Чтобы получить номер участника подтверди свою регистрацию, подтверди свою регистрацию:<br />
            <br />
            <a href="{URL_CONFIRM_BASE}/{registrant.confirmation_code}" class="button">
                Подтвердить Регистрацию
            </a><br />""")
    }

    subjects = {
        'EST' : 'Registreerimise kinnitamine',
        'ENG' : 'Registration confirmation',
        'RUS' : 'Подтверждение регистрации'
    }

    message_body = message_body_options[registrant.language]
    message = create_multipart_message(plaintext=f"""{message_body[0]}

{URL_CONFIRM_BASE}/{registrant.confirmation_code}

This is automatic message, please do not reply.""", html=f"""
    <html>
        <head>
            <title>{subjects[registrant.language]}</title>
            <style>
                a.button {{
                    background-color: #4CAF50;
                    border: none;
                    color: white;
                    padding: 15px 32px;
                    text-align: center;
                    text-decoration: none;
                    display: inline-block;
                    font-size: 12px;
                }}
            </style>
        <head>
        <body>
            {message_body[1]}
            <br />
            <br />
            Copy-Paste: {URL_CONFIRM_BASE}/{registrant.confirmation_code}.<br />
            <br />
            <i>This is automatic message, please do not reply.</i>
        </body>
    </html>""")

    message['From'] = CONFIRMATION_FROM
    message['To'] = f'{registrant.first_name} {registrant.last_name} <{registrant.email}>'
    message['Subject'] = subjects[registrant.language]

    return await send_email(message, SMTP_SERVER, TESTING)

async def send_welcome_email(participant: Participant) -> None:
    await participant.registrant.load()

    mc_audience = MAILCHIMP_AUDIENCE_OPTIONS[participant.registrant.language]
    mc_api_url = f'https://{MAILCHIMP_DATACENTER}.api.mailchimp.com/3.0/lists/{mc_audience}/members/'
    auth_value: str = b64encode(f'wgsystem:{str(MAILCHIMP_API_KEY)}'.encode('ascii')).decode('ascii')
    headers = {
        'Authorization' : f'Basic {auth_value}',
        'Content-Type' : 'application/json'
    }

    mc_subscriber_data = {
        "email_address": participant.registrant.email,
        "status": "subscribed",
        "merge_fields": {
            "FNAME": participant.registrant.first_name,
            "LNAME": participant.registrant.last_name,
            "AGE" : participant.registrant.age,
            "GENDER" : participant.registrant.gender,
            "QTY" : participant.start_number
        }
    }

    if TESTING:
        return

    await post(mc_api_url, json=mc_subscriber_data, headers=headers) # pragma: no cover
