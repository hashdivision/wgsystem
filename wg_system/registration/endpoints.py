from datetime import date
from secrets import token_urlsafe
from starlette import status
from starlette.requests import Request as StarletteRequest
from starlette.responses import PlainTextResponse, JSONResponse, RedirectResponse
from . settings import RECAPTCHA_DISABLE, RECAPTCHA_SECRET_KEY, DATE_REGISTRATION_END, \
                       URL_NOT_FOUND_PAGE, URL_WELCOME_PAGE, TESTING
from . schemas import Register
from . models import Registrant, Participant
from . mail import send_confirmation_email, send_welcome_email
from .. utils.recaptcha import is_recaptcha_passed

async def register_v0(request: StarletteRequest) -> JSONResponse:
    if date.today() >= DATE_REGISTRATION_END.date(): # pragma: no cover
        return JSONResponse({'check_date' : 'Registration is closed!'}, status.HTTP_404_NOT_FOUND)

    form = await request.form()

    if 'privacy_policy_consent' not in form or form['privacy_policy_consent'] != "1":
        return JSONResponse({'privacy_policy' : 'To register you should read and agree!'}, status.HTTP_400_BAD_REQUEST)

    if not RECAPTCHA_DISABLE: # pragma: no cover
        if 'g-recaptcha-response' not in form:
            return JSONResponse({'g-recaptcha-response' : 'ReCAPTCHA check is missing!'}, status.HTTP_401_UNAUTHORIZED)
        
        if not await is_recaptcha_passed(form['g-recaptcha-response'], str(RECAPTCHA_SECRET_KEY)):
            return JSONResponse({'g-recaptcha-response' : 'Bots not allowed!'}, status.HTTP_401_UNAUTHORIZED)

    data = Register.parse_form(form)
    register, error = Register.validate_or_error(data)
    if error:
        return JSONResponse({message.index[0]: message.text for message in error.messages()}, status.HTTP_409_CONFLICT)

    if not await register.is_email_unique():
        return JSONResponse({'email' : 'Already registered!'}, status.HTTP_403_FORBIDDEN)

    registrant = await register.create_registrant()

    code = '1234567890' if TESTING else token_urlsafe(64)
    await registrant.update(confirmation_code=code)

    if not await send_confirmation_email(registrant): # pragma: no cover
        return JSONResponse({'mail_server' : 'Mail server error!'},
                            status.HTTP_500_INTERNAL_SERVER_ERROR)

    return JSONResponse({'success' : True})

async def confirm_v0(request: StarletteRequest) -> RedirectResponse:
    if date.today() >= DATE_REGISTRATION_END.date(): # pragma: no cover
        return RedirectResponse(url=URL_NOT_FOUND_PAGE)

    if 'confirmation_code' not in request.path_params: # pragma: no cover
        return RedirectResponse(url=URL_NOT_FOUND_PAGE)

    confirmation_code = request.path_params['confirmation_code']
    registrants = await Registrant.objects.filter(confirmation_code=confirmation_code).all()
    if len(registrants) == 0 or registrants[0] is None:
        return RedirectResponse(url=URL_NOT_FOUND_PAGE)

    registrant = registrants[0]
    await registrant.update(confirmed=True, confirmation_code=None)
    participant = await Participant.objects.create(registrant=registrant, burpees_made=None)

    if not await send_welcome_email(participant): # pragma: no cover
        # LOG/REPORT ISSUE HERE (?)
        pass

    return RedirectResponse(url=f'{URL_WELCOME_PAGE}-{registrant.language.lower()}')
