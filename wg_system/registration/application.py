from starlette.applications import Starlette
from . settings import DEBUG
from . routes import ROUTES
from . resources import DATABASE, DATABASE_ENGINE, DATABASE_METADATA

REGISTRATION = Starlette(debug=DEBUG, routes=ROUTES)
# Create all tables related to registration in database
DATABASE_METADATA.create_all(DATABASE_ENGINE)

@REGISTRATION.on_event('startup')
async def startup():
    await DATABASE.connect()

@REGISTRATION.on_event('shutdown')
async def shutdown():
    await DATABASE.disconnect()
