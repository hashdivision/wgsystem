from datetime import date
from typesystem import Schema, String, Integer, Boolean
from starlette.formparsers import FormData
from . models import Registrant

EMPTY_REGISTER_DATA = {
    'first_name' : None, # type: str
    'last_name' : None, # type: str
    'email' : None, # type: str
    'age' : None, # type: int
    'gender' : None, # type: str
    'language' : None # type: str
}

class Register(Schema):
    #: [REGISTRATION FORM | REQUIRED]
    first_name = String(max_length=64, allow_blank=False)
    #: [REGISTRATION FORM | REQUIRED]
    last_name = String(max_length=64, allow_blank=False)
    #: [REGISTRATION FORM | REQUIRED]
    email = String(max_length=254, pattern=r'[^@]+@[^@]+\.[^@]+', allow_blank=False)
    #: [REGISTRATION FORM | REQUIRED]
    age = Integer(exclusive_minimum=0, maximum=100)
    #: [REGISTRATION FORM | REQUIRED]
    gender = String(max_length=16, allow_blank=False)
    #: [REGISTRATION FORM | HIDDEN]
    language = String(max_length=3, pattern=r'^EST$|^ENG$|^RUS$', allow_blank=False)

    async def is_email_unique(self) -> bool:
        registrants = await Registrant.objects.filter(email=self.email).all()
        if len(registrants) > 0 and registrants[0] is not None:
            return False

        return True

    @staticmethod
    def parse_form(form: FormData) -> dict:
        data = EMPTY_REGISTER_DATA.copy()
        data['first_name'] = form.get('first_name', default=None)
        data['last_name'] = form.get('last_name', default=None)
        data['email'] = form.get('email', default=None)
        
        try:
            data['age'] = int(form.get('age', default='0'))
        except ValueError: pass # pragma: no cover

        data['gender'] = form.get('gender', default=None)
        data['language'] = form.get('language', default=None)

        return data

    async def create_registrant(self) -> Registrant:
        return await Registrant.objects.create(language=self.language, first_name=self.first_name,
            last_name=self.last_name, email=self.email, age=self.age, gender=self.gender)
