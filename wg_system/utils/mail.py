from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from aiosmtplib import SMTP

def create_multipart_message(plaintext: str, html: str) -> MIMEMultipart:
    message = MIMEMultipart('alternative')
    message.attach(MIMEText(plaintext, 'plain', 'utf-8'))
    message.attach(MIMEText(html, 'html', 'utf-8'))

    return message

async def send_email(message: MIMEMultipart, smtp_server: SMTP, is_testing: bool = False) -> bool:
    if is_testing:
        return True

    try:
        await smtp_server.connect()
        await smtp_server.send_message(message)
        await smtp_server.quit()
    except:
        # LOG/REPORT ISSUE HERE (?)
        return False

    return True
